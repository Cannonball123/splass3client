#include <stdlib.h>
#include <boost/locale.hpp>
#include "connectionHandler.h"
#include <boost/thread.hpp>
void workFunk(ConnectionHandler*);
/**
* This code assumes that the server replies the exact text the client sent it (as opposed to the practical session example)
*/
bool quitted = false;

void workFunk(ConnectionHandler* connectionHandler) {
    std::cout<<"Input Thread started..."<<std::endl;

    //From here we will see the rest of the ehco client implementation:
    while (1) {
        // We can use one of three options to read data from the server:
        // 1. Read a fixed number of characters
        // 2. Read a line (up to the newline character using the getline() buffered reader
        // 3. Read up to the null character
        std::string answer;
        // Get back an answer: by using the expected number of bytes (len bytes + newline delimiter)
        // We could also use: connectionHandler.getline(answer) and then get the answer without the newline char at the end
        if (!connectionHandler->getLine(answer)) {
            std::cout << "Disconnected. Exiting...\n" << std::endl;
            break;
        }


        int  len=answer.length();
        // A C string must end with a 0 char delimiter.  When we filled the answer buffer from the socket
        // we filled up to the \n char - we must make sure now that a 0 char is also present. So we truncate last character.
        //answer.resize(len-1);
        //std::cout << "Reply: " << answer << " " << len << " bytes " << std::endl << std::endl;p
        std::cout << answer << std::endl;
        std::string quit = "SYSMSG QUIT ACCEPTED\r\n";

        if (answer.compare(quit)==0) {
            quitted = true;
            std::cout << "Quit was issued\n" << std::endl;
            break;
        }
    }
}


int main (int argc, char *argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " host port" << std::endl << std::endl;
		std::cerr << "Press any key to exit..." << std::endl << std::endl;
		std::cin.get();
        return -1;
    }

    std::string host = argv[1];
	if (strcmp(argv[1], "localhost") == 0) {
		host = std::string("127.0.0.1");
	}
	short port = atoi(argv[2]);
    
    ConnectionHandler* connectionHandler = new ConnectionHandler (host, port);
    if (!connectionHandler->connect()) {
        std::cerr << "Cannot connect to "<< std::endl;
    }
    else
	{
        boost::thread runner(boost::bind(workFunk, connectionHandler));

        while (!quitted) {

            //std::cout << "Please insert command....." << std::endl;
            std::string line;
            std::getline(std::cin, line);

            int len = line.length();
            if (!connectionHandler->sendLine(line)) {
                std::cout << "Disconnected. Exiting...\n" << std::endl;
                break;
            }
            //std::cout << "Sent " << len + 1 << " bytes to server" << std::endl;

        }

		std::cout << "Exiting...\n" << std::endl;

        //std::cout << "before join" << std::endl;
        //runner.join();
    }

	std::cin.get();
    return 0;
}
